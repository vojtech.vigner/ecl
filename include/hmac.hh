/*!
 * \file:   hmac.hh
 * \author: Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:   2022-06-24
 * \brief:  HMAC-SHA-224/256/384/512 implementation based on Olivier Gay's
 * library https://github.com/ogay/hmac
 *
 * \attention The BSD License 2.0
 *
 * Copyright (c) 2022 Vojtech Vigner, vojtech.vigner@gmail.com
 * Copyright (C) 2005 Olivier Gay <olivier.gay@a3.epfl.ch>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef HMAC_HH
#define HMAC_HH

#include "sha.hh"

#include <stdint.h>
#include <string.h>

namespace ecl {
namespace HMAC {
using Version = SHA::Version;
template <Version V>
class Processor {
 public:
  Processor() { reset(nullptr, 0); }
  Processor(const uint8_t *key, int key_size) { reset(key, key_size); }
  Processor(const Processor &) = delete;

  void reset(const uint8_t *key, int key_size) {
    memset(_ipad, 0x36, SHA::Processor<V>::getBlockSize());
    memset(_opad, 0x5C, SHA::Processor<V>::getBlockSize());

    for (int i = 0; i < key_size; i++) {
      _ipad[i] ^= key[i];
      _opad[i] ^= key[i];
    }

    _sha.update(_ipad, SHA::Processor<V>::getBlockSize());
  }
  void update(const uint8_t *message, int length) {
    _sha.update(message, length);
  }
  void finish(int size) {
    _sha.finish(_result);
    _sha.reset();
    update(_opad, SHA::Processor<V>::getBlockSize());
    update(_result, size);
    _sha.finish(_result);
  }

  void getResult(uint8_t *output, int size) const {
    memcpy(output, _result, size);
  }

  bool compareResult(const uint8_t *input, int size) const {
    return memcmp(input, _result, size) == 0;
  }

 private:
  SHA::Processor<V> _sha;
  uint8_t _ipad[SHA::Processor<V>::getBlockSize()];
  uint8_t _opad[SHA::Processor<V>::getBlockSize()];
  uint8_t _result[SHA::Processor<V>::getDigestSize()];
};

template <Version V>
void hash(const uint8_t *key, int key_size, const uint8_t *message,
          int message_length, uint8_t *mac, int mac_size) {
  Processor<V> p(key, key_size);
  p.update(message, message_length);
  p.finish(mac_size);
  p.getResult(mac, mac_size);
}

}  // namespace HMAC
}  // namespace ecl

#endif /* !HMAC_HH */
