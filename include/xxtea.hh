/*!
 * \file:   xxtea.hh
 * \author: Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:   2022-06-24
 * \brief:  XXTEA with CBC
 *
 * \attention The BSD License 2.0
 * Copyright (c) 2022 Vojtech Vigner, vojtech.vigner@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef XXTEA_HH
#define XXTEA_HH

#include <stdint.h>
#include <string.h>

namespace ecl {
class XXTEA {
 public:
  template <typename T, int S>
  class Array {
   public:
    Array(T const x[S]) : _x({x}){};
    Array(){};

    T &operator[](int index) { return _x[index]; }
    const T &operator[](int index) const { return _x[index]; }
    T *get() { return _x; }

   private:
    T _x[S];
  };

  using IV = Array<uint32_t, 4>;
  using Key = Array<uint32_t, 3>;

 private:
  static constexpr uint32_t kDelta = 0x9e3779b9;
  static constexpr uint32_t mx(uint32_t y, uint32_t z, uint32_t s, uint32_t e,
                               uint32_t k) {
    return (((z >> 5) ^ (y << 2)) + ((y >> 3) ^ (z << 4))) ^
           ((s ^ y) + (k ^ z));
  }

  Key _key;
  mutable IV _iv;

 public:
  XXTEA(const Key &key, const IV &iv) : _key(key), _iv(iv){};
  XXTEA(const Key &key) : _key(key){};

  void setKey(const Key &key) { _key = key; }
  void setIV(const IV &iv) { _iv = iv; }

  bool decrypt(const uint8_t *input, int size, uint8_t *output) const {
    uint32_t temp[4];

    if (size % 16) return false;

    const uint32_t *input32 = reinterpret_cast<const uint32_t *>(input);
    uint32_t *output32 = reinterpret_cast<uint32_t *>(output);

    while (size > 0) {
      memcpy(temp, input32, 16);
      memcpy(output32, input32, 16);

      const uint32_t n = 3;
      uint32_t z, p, e;
      uint32_t y = output32[0];
      uint32_t q = 6 + 52 / (n + 1);
      uint32_t sum = q * kDelta;

      while (sum != 0) {
        e = sum >> 2 & 3;

        for (p = n; p > 0; p--) {
          z = output32[p - 1];
          y = output32[p] -= mx(y, z, sum, e, _key[(p & 3) ^ e]);
        }

        z = output32[n];
        y = output32[0] -= mx(y, z, sum, e, _key[(p & 3) ^ e]);
        sum -= kDelta;
      }

      for (int i = 0; i < 4; i++) output32[i] = output32[i] ^ _iv[i];

      memcpy(_iv.get(), temp, 16);

      input32 += 4;
      output32 += 4;
      size -= 16;
    }

    return true;
  }

  bool encrypt(const uint8_t *input, int size, uint8_t *output) {
    if (size % 16) return false;

    const uint32_t *input32 = reinterpret_cast<const uint32_t *>(input);
    uint32_t *output32 = reinterpret_cast<uint32_t *>(output);

    while (size > 0) {
      for (int i = 0; i < 4; i++) output32[i] = input32[i] ^ _iv[i];

      const uint32_t n = 3;
      uint32_t z = output32[n], y, p, q = 6 + 52 / (n + 1), sum = 0, e;

      while (0 < q--) {
        sum += kDelta;
        e = sum >> 2 & 3;

        for (p = 0; p < n; p++) {
          y = output32[p + 1];
          z = output32[p] += mx(y, z, sum, e, _key[(p & 3) ^ e]);
        }

        y = output32[0];
        z = output32[n] += mx(y, z, sum, e, _key[(p & 3) ^ e]);
      }

      memcpy(_iv.get(), output32, 16);

      input32 += 4;
      output32 += 4;
      size -= 16;
    }

    return true;
  }
};
}  // namespace ecl

#endif /* !XXTEA_HH */