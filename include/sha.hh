/*!
 * \file:   sha.hh
 * \author: Vojtech Vigner, vojtech.vigner@gmail.com
 * \date:   2022-06-24
 * \brief:  SHA embedded library based on Olivier Gay's library
 * https://github.com/ogay/hmac
 *
 * \attention The BSD License 2.0
 * Copyright (c) 2022 Vojtech Vigner, vojtech.vigner@gmail.com
 * Copyright (C) 2005, 2007 Olivier Gay <olivier.gay@a3.epfl.ch>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef SHA_HH
#define SHA_HH

#include <stdint.h>
#include <string.h>

namespace ecl {
namespace SHA {
enum class Version { SHA224, SHA256, SHA384, SHA512 };

/* 256 */
template <Version V = Version::SHA256>
struct Specialization {
  using h_type = uint32_t;
  static constexpr int kDigestSize = 256 / 8;
  static constexpr int kBlockSize = 512 / 8;
  static constexpr int kKSize = 64;
  static constexpr const h_type kK[kKSize] = {
      0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
      0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
      0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
      0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
      0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
      0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
      0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
      0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
      0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
      0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
      0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
  static constexpr const h_type kH0[8] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372,
                                          0xa54ff53a, 0x510e527f, 0x9b05688c,
                                          0x1f83d9ab, 0x5be0cd19};

  static inline h_type rotr(h_type x, h_type n) {
    return ((x >> n) | (x << ((sizeof(x) << 3) - n)));
  }
  static inline h_type f1(h_type x) {
    return (rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22));
  }
  static inline h_type f2(h_type x) {
    return (rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25));
  }
  static inline h_type f3(h_type x) {
    return (rotr(x, 7) ^ rotr(x, 18) ^ (x >> 3));
  }
  static inline h_type f4(h_type x) {
    return (rotr(x, 17) ^ rotr(x, 19) ^ (x >> 10));
  }
};

/* 512 */
template <>
struct Specialization<Version::SHA512> {
  using h_type = uint64_t;
  static constexpr int kDigestSize = 512 / 8;
  static constexpr int kBlockSize = 1024 / 8;
  static constexpr int kKSize = 80;
  static constexpr const h_type kK[kKSize] = {
      0x428a2f98d728ae22ULL, 0x7137449123ef65cdULL, 0xb5c0fbcfec4d3b2fULL,
      0xe9b5dba58189dbbcULL, 0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL,
      0x923f82a4af194f9bULL, 0xab1c5ed5da6d8118ULL, 0xd807aa98a3030242ULL,
      0x12835b0145706fbeULL, 0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL,
      0x72be5d74f27b896fULL, 0x80deb1fe3b1696b1ULL, 0x9bdc06a725c71235ULL,
      0xc19bf174cf692694ULL, 0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL,
      0x0fc19dc68b8cd5b5ULL, 0x240ca1cc77ac9c65ULL, 0x2de92c6f592b0275ULL,
      0x4a7484aa6ea6e483ULL, 0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL,
      0x983e5152ee66dfabULL, 0xa831c66d2db43210ULL, 0xb00327c898fb213fULL,
      0xbf597fc7beef0ee4ULL, 0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL,
      0x06ca6351e003826fULL, 0x142929670a0e6e70ULL, 0x27b70a8546d22ffcULL,
      0x2e1b21385c26c926ULL, 0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL,
      0x650a73548baf63deULL, 0x766a0abb3c77b2a8ULL, 0x81c2c92e47edaee6ULL,
      0x92722c851482353bULL, 0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL,
      0xc24b8b70d0f89791ULL, 0xc76c51a30654be30ULL, 0xd192e819d6ef5218ULL,
      0xd69906245565a910ULL, 0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL,
      0x19a4c116b8d2d0c8ULL, 0x1e376c085141ab53ULL, 0x2748774cdf8eeb99ULL,
      0x34b0bcb5e19b48a8ULL, 0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL,
      0x5b9cca4f7763e373ULL, 0x682e6ff3d6b2b8a3ULL, 0x748f82ee5defb2fcULL,
      0x78a5636f43172f60ULL, 0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL,
      0x90befffa23631e28ULL, 0xa4506cebde82bde9ULL, 0xbef9a3f7b2c67915ULL,
      0xc67178f2e372532bULL, 0xca273eceea26619cULL, 0xd186b8c721c0c207ULL,
      0xeada7dd6cde0eb1eULL, 0xf57d4f7fee6ed178ULL, 0x06f067aa72176fbaULL,
      0x0a637dc5a2c898a6ULL, 0x113f9804bef90daeULL, 0x1b710b35131c471bULL,
      0x28db77f523047d84ULL, 0x32caab7b40c72493ULL, 0x3c9ebe0a15c9bebcULL,
      0x431d67c49c100d4cULL, 0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL,
      0x5fcb6fab3ad6faecULL, 0x6c44198c4a475817ULL};
  static constexpr const h_type kH0[8] = {
      0x6a09e667f3bcc908ULL, 0xbb67ae8584caa73bULL, 0x3c6ef372fe94f82bULL,
      0xa54ff53a5f1d36f1ULL, 0x510e527fade682d1ULL, 0x9b05688c2b3e6c1fULL,
      0x1f83d9abfb41bd6bULL, 0x5be0cd19137e2179ULL};

  static inline h_type rotr(h_type x, h_type n) {
    return ((x >> n) | (x << ((sizeof(x) << 3) - n)));
  }

  static inline h_type f1(h_type x) {
    return (rotr(x, 28) ^ rotr(x, 34) ^ rotr(x, 39));
  }

  static inline h_type f2(h_type x) {
    return (rotr(x, 14) ^ rotr(x, 18) ^ rotr(x, 41));
  }

  static inline h_type f3(h_type x) {
    return (rotr(x, 1) ^ rotr(x, 8) ^ (x >> 7));
  }

  static inline h_type f4(h_type x) {
    return (rotr(x, 19) ^ rotr(x, 61) ^ (x >> 6));
  }
};

/* 384 */
template <>
struct Specialization<Version::SHA384> {
  using h_type = uint64_t;
  static constexpr int kDigestSize = 384 / 8;
  static constexpr int kBlockSize = Specialization<Version::SHA512>::kBlockSize;
  static constexpr int kKSize = Specialization<Version::SHA512>::kKSize;
  static constexpr const h_type (&kK)[kKSize] =
      Specialization<Version::SHA512>::kK;
  static constexpr const h_type kH0[8] = {
      0xcbbb9d5dc1059ed8ULL, 0x629a292a367cd507ULL, 0x9159015a3070dd17ULL,
      0x152fecd8f70e5939ULL, 0x67332667ffc00b31ULL, 0x8eb44a8768581511ULL,
      0xdb0c2e0d64f98fa7ULL, 0x47b5481dbefa4fa4ULL};

  static inline h_type rotr(h_type x, h_type n) {
    return Specialization<Version::SHA512>::rotr(x, n);
  }
  static inline h_type f1(h_type x) {
    return Specialization<Version::SHA512>::f1(x);
  }
  static inline h_type f2(h_type x) {
    return Specialization<Version::SHA512>::f2(x);
  }
  static inline h_type f3(h_type x) {
    return Specialization<Version::SHA512>::f3(x);
  }
  static inline h_type f4(h_type x) {
    return Specialization<Version::SHA512>::f4(x);
  }
};

/* 224 */
template <>
struct Specialization<Version::SHA224> {
  using h_type = uint32_t;
  static constexpr int kDigestSize = 224 / 8;
  static constexpr int kBlockSize = Specialization<Version::SHA256>::kBlockSize;
  static constexpr int kKSize = Specialization<Version::SHA256>::kKSize;
  static constexpr const h_type (&kK)[kKSize] =
      Specialization<Version::SHA256>::kK;
  static constexpr const h_type kH0[8] = {0xc1059ed8, 0x367cd507, 0x3070dd17,
                                          0xf70e5939, 0xffc00b31, 0x68581511,
                                          0x64f98fa7, 0xbefa4fa4};

  static inline h_type rotr(h_type x, h_type n) {
    return Specialization<Version::SHA256>::rotr(x, n);
  }
  static inline h_type f1(h_type x) {
    return Specialization<Version::SHA256>::f1(x);
  }
  static inline h_type f2(h_type x) {
    return Specialization<Version::SHA256>::f2(x);
  }
  static inline h_type f3(h_type x) {
    return Specialization<Version::SHA256>::f3(x);
  }
  static inline h_type f4(h_type x) {
    return Specialization<Version::SHA256>::f4(x);
  }
};

template <Version V>
class Processor {
 public:
  Processor() { reset(); }
  Processor(const Processor &) = delete;

  static constexpr int getDigestSize() {
    return Specialization<V>::kDigestSize;
  }
  static constexpr int getBlockSize() { return Specialization<V>::kBlockSize; }

  void reset() {
    for (int i = 0; i < 8; i++) {
      _h[i] = kH0[i];
    }

    _length = 0;
    _total_length = 0;
  }

  void update(const uint8_t *message, int length) {
    unsigned int block_nb;
    unsigned int new_len, rem_len, tmp_len;
    const unsigned char *shifted_message;

    constexpr int shift =
        ((V == Version::SHA384) || (V == Version::SHA512)) ? 7 : 6;

    tmp_len = getBlockSize() - _length;
    rem_len = length < tmp_len ? length : tmp_len;

    memcpy(&_block[_length], message, rem_len);

    if (_length + length < getBlockSize()) {
      _length += length;
      return;
    }

    new_len = length - rem_len;
    block_nb = new_len / getBlockSize();

    shifted_message = message + rem_len;

    transf(_block, 1);
    transf(shifted_message, block_nb);

    rem_len = new_len % getBlockSize();

    memcpy(_block, &shifted_message[block_nb << shift], rem_len);

    _length = rem_len;
    _total_length += (block_nb + 1) << shift;
  }

  void finish(uint8_t (&digest)[getDigestSize()]) {
    constexpr int dec =
        ((V == Version::SHA384) || (V == Version::SHA512)) ? 17 : 9;
    const unsigned int block_nb =
        (1 + ((getBlockSize() - dec) < (_length % getBlockSize())));
    const unsigned int len_b = (_total_length + _length) << 3;
    constexpr int shift =
        ((V == Version::SHA384) || (V == Version::SHA512)) ? 7 : 6;
    const unsigned int pm_len = block_nb << shift;

    memset(_block + _length, 0, pm_len - _length);
    _block[_length] = 0x80;

    pack32(len_b, _block + pm_len - 4);

    transf(_block, block_nb);

    constexpr int size = (V == Version::SHA384)   ? 6
                         : (V == Version::SHA224) ? 7
                                                  : 8;
    for (int i = 0; i < size; i++) {
      if ((V == Version::SHA384) || (V == Version::SHA512)) {
        pack64(_h[i], &digest[i << 3]);
      } else {
        pack32(_h[i], &digest[i << 2]);
      }
    }
  }

 private:
  using h_type = typename Specialization<V>::h_type;
  static constexpr const h_type (&kH0)[8] = Specialization<V>::kH0;
  static constexpr int getKSize() { return Specialization<V>::kKSize; }

  static constexpr const h_type (&kK)[getKSize()] = Specialization<V>::kK;

 private:
  h_type _h[8];
  int _total_length;
  int _length;
  uint8_t _block[2 * getBlockSize()];

 private:
  static inline void pack32(const uint32_t x, uint8_t *dst) {
    uint32_t tmp = ((x << 8) & 0xFF00FF00) | ((x >> 8) & 0xFF00FF);
    *(reinterpret_cast<uint32_t *>(dst)) = (tmp << 16) | (tmp >> 16);
  }

  static inline void pack64(const uint64_t x, uint8_t *dst) {
    uint64_t tmp = x;
    tmp = ((tmp & 0x00000000FFFFFFFFull) << 32) |
          ((tmp & 0xFFFFFFFF00000000ull) >> 32);
    tmp = ((tmp & 0x0000FFFF0000FFFFull) << 16) |
          ((tmp & 0xFFFF0000FFFF0000ull) >> 16);
    tmp = ((tmp & 0x00FF00FF00FF00FFull) << 8) |
          ((tmp & 0xFF00FF00FF00FF00ull) >> 8);
    *(reinterpret_cast<uint64_t *>(dst)) = tmp;
  }

  void transf(const uint8_t *message, unsigned int block_nb) {
    h_type w[getKSize()];
    h_type wv[8];
    h_type t1, t2;
    const unsigned char *sub_block;

    for (int i = 0; i < (int)block_nb; i++) {
      if ((V == Version::SHA384) || (V == Version::SHA512))
        sub_block = message + (i << 7);
      else
        sub_block = message + (i << 6);

      for (int j = 0; j < 16; j++) {
        if ((V == Version::SHA384) || (V == Version::SHA512)) {
          pack64(*(reinterpret_cast<const uint64_t *>(&sub_block[j << 3])),
                 reinterpret_cast<uint8_t *>(&w[j]));
        } else {
          pack32(*(reinterpret_cast<const uint32_t *>(&sub_block[j << 2])),
                 reinterpret_cast<uint8_t *>(&w[j]));
        }
      }

      for (int j = 16; j < getKSize(); j++) {
        w[j] = Specialization<V>::f4(w[j - 2]) + w[j - 7] +
               Specialization<V>::f3(w[j - 15]) + w[j - 16];
      }

      for (int j = 0; j < 8; j++) {
        wv[j] = _h[j];
      }

      for (int j = 0; j < getKSize(); j++) {
        h_type ch = (wv[4] & wv[5]) ^ (~wv[4] & wv[6]);
        t1 = wv[7] + Specialization<V>::f2(wv[4]) + ch + kK[j] + w[j];

        h_type maj = (wv[0] & wv[1]) ^ (wv[0] & wv[2]) ^ (wv[1] & wv[2]);
        t2 = Specialization<V>::f1(wv[0]) + maj;
        wv[7] = wv[6];
        wv[6] = wv[5];
        wv[5] = wv[4];
        wv[4] = wv[3] + t1;
        wv[3] = wv[2];
        wv[2] = wv[1];
        wv[1] = wv[0];
        wv[0] = t1 + t2;
      }

      for (int j = 0; j < 8; j++) {
        _h[j] += wv[j];
      }
    }
  }
};

template <Version V>
void hash(const uint8_t *message, int length,
          uint8_t (&output)[Processor<V>::getDigestSize()]) {
  Processor<V> p;
  p.update(message, length);
  p.finish(output);
}

}  // namespace SHA

}  // namespace ecl

#endif /* !SHA_HH */