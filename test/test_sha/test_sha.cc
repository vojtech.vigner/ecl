#include <unity.h>

#include <memory>
#include <string>
#include <vector>

#include "sha.hh"

static std::vector<std::string> _vectors_224{
    "23097d223405d8228642a477bda255b32aadbce4bda0b3f7e36c9da7",
    "75388b16512776cc5dba5da1fd890150b0c6455cb4f58b1952522525",
    "20794655980c91d8bbb4c1ea97618a4bf03f42581948b2ee4ee7ad67",
};

static std::vector<std::string> _vectors_256{
    "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad",
    "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1",
    "cdc76e5c9914fb9281a1c7e284d73e67f1809a48a497200e046d39ccc7112cd0",
};
static std::vector<std::string> _vectors_384{
    "cb00753f45a35e8bb5a03d699ac65007272c32ab0eded1631a8b605a43ff5bed"
    "8086072ba1e7cc2358baeca134c825a7",
    "09330c33f71147e83d192fc782cd1b4753111b173b3b05d22fa08086e3b0f712"
    "fcc7c71a557e2db966c3e9fa91746039",
    "9d0e1809716474cb086e834e310a4a1ced149e9c00f248527972cec5704c2a5b"
    "07b8b3dc38ecc4ebae97ddd87f3d8985",
};
static std::vector<std::string> _vectors_512{
    "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a"
    "2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f",
    "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018"
    "501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909",
    "e718483d0ce769644e2e42c7bc15b4638e1f98b13b2044285632a803afa973eb"
    "de0ff244877ea60a4cb0432ce577c31beb009c5c2c49aa2e4eadb217ad8cc09b"};

static std::vector<std::string> _message{
    "abc",
    "abcdbcdecdefdefgefghfghighijhi"
    "jkijkljklmklmnlmnomnopnopq",
    "abcdefghbcdefghicdefghijdefghijkefghij"
    "klfghijklmghijklmnhijklmnoijklmnopjklm"
    "nopqklmnopqrlmnopqrsmnopqrstnopqrstu",
    std::string(1000000, 'a')};

template <ecl::SHA::Version V>
static bool validateSHA(const std::string &plain, const std::string &result) {
  constexpr int kDigestSize = ecl::SHA::Processor<V>::getDigestSize();
  uint8_t digest[kDigestSize];

  ecl::SHA::hash<V>(reinterpret_cast<const uint8_t *>(plain.data()),
                    plain.size(), digest);

  std::string text;
  text.reserve(kDigestSize * 2 + 1);
  for (int i = 0; i < kDigestSize; i++) {
    char hex[3];
    snprintf(hex, sizeof(hex), "%02x", digest[i]);
    text.append(hex, 2);
  }
  text.append("\0");

  return 0 == result.compare(text);
}

template <ecl::SHA::Version V>
static void testSHA() {
  using Version = ecl::SHA::Version;
  auto vectors = V == Version::SHA224   ? _vectors_224
                 : V == Version::SHA256 ? _vectors_256
                 : V == Version::SHA384 ? _vectors_384
                                        : _vectors_512;

  auto message2 = V == Version::SHA224   ? _message[1]
                  : V == Version::SHA256 ? _message[1]
                  : V == Version::SHA384 ? _message[2]
                                         : _message[2];

  TEST_ASSERT(validateSHA<V>(_message[0], vectors[0]));
  TEST_ASSERT(validateSHA<V>(message2, vectors[1]));
  TEST_ASSERT(validateSHA<V>(_message[3], vectors[2]));
}

static void testSHA_224() { testSHA<ecl::SHA::Version::SHA224>(); }
static void testSHA_256() { testSHA<ecl::SHA::Version::SHA256>(); }
static void testSHA_384() { testSHA<ecl::SHA::Version::SHA384>(); }
static void testSHA_512() { testSHA<ecl::SHA::Version::SHA512>(); }

void setUp(void) {}

void tearDown(void) {}

int main(void) {
  UNITY_BEGIN();
  RUN_TEST(testSHA_224);
  RUN_TEST(testSHA_256);
  RUN_TEST(testSHA_384);
  RUN_TEST(testSHA_512);
  return UNITY_END();
}
