#include <unity.h>

#include <memory>
#include <string>
#include <vector>
#include <random>
#include <algorithm>

#include "xxtea.hh"

static void testXXTEA() {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<uint8_t> dist8;

  std::vector<uint8_t> bin(1024);
  std::vector<uint8_t> encrypted(1024);
  std::vector<uint8_t> decrypted(1024);

  std::generate(bin.begin(), bin.end(), [&]() { return dist8(mt); });

  constexpr ecl::XXTEA::IV iv = {0x23097d22, 0x3405d822, 0x8642a477,
                                 0xbda255b3};
  constexpr ecl::XXTEA::Key key = {0x12345678, 0xAAAABBBB, 0x87654321};

  ecl::XXTEA::encrypt(bin.data(), bin.size(), encrypted.data(), iv, key);

  ecl::XXTEA::decrypt(encrypted.data(), encrypted.size(), decrypted.data(), iv,
                      key);

  bool result = std::equal(bin.begin(), bin.end(), decrypted.begin());

  TEST_ASSERT(result);
}

void setUp(void) {}

void tearDown(void) {}

int main(void) {
  UNITY_BEGIN();
  RUN_TEST(testXXTEA);
  return UNITY_END();
}
