#include "hmac.hh"

#include <unity.h>

#include <memory>
#include <string>
#include <vector>

static std::vector<std::string> _vectors_224{
    "896fb1128abbdf196832107cd49df33f47b4b1169912ba4f53684b22",
    "a30e01098bc6dbbf45690f3a7e9e6d0f8bbea2a39e6148008fd05e44",
    "7fb3cb3588c6c1f6ffa9694d7d6ad2649365b0c1f65d69d1ec8333ea",
    "6c11506874013cac6a2abc1bb382627cec6a90d86efc012de7afec5a",
    "0e2aea68a90c8d37c988bcdb9fca6fa8",
    "95e9a0db962095adaebe9b2d6f0dbce2d499f112f2d2b7273fa6870e",
    "3a854166ac5d9f023f54d517d0b39dbd946770db9c2b95c9f6f565d1"};

static std::vector<std::string> _vectors_256{
    "b0344c61d8db38535ca8afceaf0bf12b881dc200c9833da726e9376c2e32cff7",
    "5bdcc146bf60754e6a042426089575c75a003f089d2739839dec58b964ec3843",
    "773ea91e36800e46854db8ebd09181a72959098b3ef8c122d9635514ced565fe",
    "82558a389a443c0ea4cc819899f2083a85f0faa3e578f8077a2e3ff46729665b",
    "a3b6167473100ee06e0c796c2955552b",
    "60e431591ee0b67f0d8a26aacbf5b77f8e0bc6213728c5140546040f0ee37f54",
    "9b09ffa71b942fcb27635fbcd5b0e944bfdc63644f0713938a7f51535c3a35e2"};

static std::vector<std::string> _vectors_384{
    "afd03944d84895626b0825f4ab46907f15f9dadbe4101ec682aa034c7cebc59c"
    "faea9ea9076ede7f4af152e8b2fa9cb6",
    "af45d2e376484031617f78d2b58a6b1b9c7ef464f5a01b47e42ec3736322445e"
    "8e2240ca5e69e2c78b3239ecfab21649",
    "88062608d3e6ad8a0aa2ace014c8a86f0aa635d947ac9febe83ef4e55966144b"
    "2a5ab39dc13814b94e3ab6e101a34f27",
    "3e8a69b7783c25851933ab6290af6ca77a9981480850009cc5577c6e1f573b4e"
    "6801dd23c4a7d679ccf8a386c674cffb",
    "3abf34c3503b2a23a46efc619baef897",
    "4ece084485813e9088d2c63a041bc5b44f9ef1012a2b588f3cd11f05033ac4c6"
    "0c2ef6ab4030fe8296248df163f44952",
    "6617178e941f020d351e2f254e8fd32c602420feb0b8fb9adccebb82461e99c5"
    "a678cc31e799176d3860e6110c46523e"};

static std::vector<std::string> _vectors_512{
    "87aa7cdea5ef619d4ff0b4241a1d6cb02379f4e2ce4ec2787ad0b30545e17cde"
    "daa833b7d6b8a702038b274eaea3f4e4be9d914eeb61f1702e696c203a126854",
    "164b7a7bfcf819e2e395fbe73b56e0a387bd64222e831fd610270cd7ea250554"
    "9758bf75c05a994a6d034f65f8f0e6fdcaeab1a34d4a6b4b636e070a38bce737",
    "fa73b0089d56a284efb0f0756c890be9b1b5dbdd8ee81a3655f83e33b2279d39"
    "bf3e848279a722c806b485a47e67c807b946a337bee8942674278859e13292fb",
    "b0ba465637458c6990e5a8c5f61d4af7e576d97ff94b872de76f8050361ee3db"
    "a91ca5c11aa25eb4d679275cc5788063a5f19741120c4f2de2adebeb10a298dd",
    "415fad6271580a531d4179bc891d87a6",
    "80b24263c7c1a3ebb71493c1dd7be8b49b46d1f41b4aeec1121b013783f8f352"
    "6b56d037e05f2598bd0fd2215d6a1e5295e64f73f63f0aec8b915a985d786598",
    "e37b6a775dc87dbaa4dfa9f96e5e3ffddebd71f8867289865df5a32d20cdc944"
    "b6022cac3c4982b10d5eeb55c3e4de15134676fb6de0446065c97440fa8c6a58"};

static std::vector<std::string> _messages{
    "Hi There",
    "what do ya want for nothing?",
    std::string(50, 0xDD),
    std::string(50, 0xCD),
    "Test With Truncation",
    "Test Using Larger Than Block-Size Key - Hash Key First",
    "This is a test using a larger than block-size key "
    "and a larger than block-size data. The key needs"
    " to be hashed before being used by the HMAC algorithm."};

static std::vector<std::string> _keys{
    std::string(20, 0x0B), "Jefe",
    std::string(20, 0xAA), std::string(25, 0x00),
    std::string(20, 0x0C), std::string(131, 0xAA),
    std::string(131, 0xAA)};

template <ecl::HMAC::Version V>
static bool validateHMAC(const std::string &key, const std::string &message,
                         const std::string &result,
                         int size = ecl::SHA::Specialization<V>::kDigestSize) {
  std::basic_string<uint8_t> mac(size, '\0');
  ecl::HMAC::hash<V>(reinterpret_cast<const uint8_t *>(key.c_str()),
                     key.length(),
                     reinterpret_cast<const uint8_t *>(message.c_str()),
                     message.length(), mac.data(), mac.size());

  std::string text;
  text.reserve(size * 2 + 1);
  for (int i = 0; i < size; i++) {
    char hex[3];
    snprintf(hex, sizeof(hex), "%02x", mac[i]);
    text.append(hex, 2);
  }
  text.append("\0");

  return 0 == result.compare(text);
}

template <ecl::HMAC::Version V>
static void testHMAC() {
  using Version = ecl::HMAC::Version;
  auto vectors = V == Version::SHA224   ? _vectors_224
                 : V == Version::SHA256 ? _vectors_256
                 : V == Version::SHA384 ? _vectors_384
                                        : _vectors_512;
  int size = ecl::SHA::Specialization<V>::kDigestSize;
  for (int i = 0; i < 3; i++) {
    if (i == 4) size = 128 / 8;

    TEST_ASSERT(validateHMAC<V>(_keys[i], _messages[i], vectors[i], size));
  }
}

static void testHMAC_224() { testHMAC<ecl::HMAC::Version::SHA224>(); }
static void testHMAC_256() { testHMAC<ecl::HMAC::Version::SHA256>(); }
static void testHMAC_384() { testHMAC<ecl::HMAC::Version::SHA384>(); }
static void testHMAC_512() { testHMAC<ecl::HMAC::Version::SHA512>(); }

void setUp(void) {
  for (int i = 0; i < _keys[3].length(); i++) {
    _keys[3][i] = i + 1;
  }
}

void tearDown(void) {}

int main(void) {
  UNITY_BEGIN();
  RUN_TEST(testHMAC_224);
  RUN_TEST(testHMAC_256);
  RUN_TEST(testHMAC_384);
  RUN_TEST(testHMAC_512);
  return UNITY_END();
}
